"""Monitor z21 data records using asyncio.
"""

import asyncio

# import module z21 from import package z21_lan
from z21_lan import z21


class ClientDatagramProtocol(asyncio.DatagramProtocol):
    def __init__(self, on_con_lost):
        self.on_con_lost = on_con_lost

    def connection_made(self, transport):
        print("[connection_made]")
        self.transport = transport

    def datagram_received(self, data, addr):
        print("[datagram_received]", data)
        z21.z21_decode(data)

    def connection_lost(self, exc):
        print("[connection_lost]")
        self.on_con_lost.set_result(True)


async def subscribe(transport):
    """Subscribe for notifications."""
    while True:
        print("[subscribe]")

        # subscribe
        # dataLen = int.to_bytes(0x0008, 2, BYTEORDER_LITTLE)
        # header = int.to_bytes(0x0050, 2, BYTEORDER_LITTLE)
        # data = int.to_bytes(0x00010001, 4, BYTEORDER_LITTLE)
        data = int.to_bytes(
            z21.FLAG_DRIVING_SWITCHING | z21.FLAG_ALL_LOCOS | z21.FLAG_SYSTEMSTATUS,
            4,
            z21.BYTEORDER_LITTLE,
        )
        command = z21.z21_encode(z21.LAN_SET_BROADCASTFLAGS, data=data)
        transport.sendto(command)

        # every 60 seconds
        await asyncio.sleep(60)


async def main():
    """Main loop to monitor z21 messages.

    Send some basic commands to z21 central station and subscribe for notifications.
    """
    # based on example:
    # https://docs.python.org/3/library/asyncio-protocol.html#udp-echo-client

    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()

    # create a datagram (UDP) connection
    print("calling create_datagram_endpoint")
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: ClientDatagramProtocol(on_con_lost),
        remote_addr=("192.168.0.111", 21105),
    )
    print("called create_datagram_endpoint")

    # serial number
    command = z21.z21_encode(z21.LAN_GET_SERIAL_NUMBER)
    transport.sendto(command)

    # code
    command = z21.z21_encode(z21.LAN_GET_CODE)
    transport.sendto(command)

    # hardware info
    command = z21.z21_encode(z21.LAN_GET_HWINFO)
    transport.sendto(command)

    # x-bus version
    command = z21.z21_encode(z21.LAN_X_GET_VERSION)
    transport.sendto(command)

    # firmware version
    command = z21.z21_encode(z21.LAN_X_GET_FIRMWARE_VERSION)
    transport.sendto(command)

    # system state
    command = z21.z21_encode(z21.LAN_SYSTEMSTATE_GETDATA)
    transport.sendto(command)

    subscribe_task = asyncio.create_task(subscribe(transport))

    try:
        # run functions: on_con_lost, subscribe_task
        await asyncio.gather(on_con_lost, subscribe_task)
    finally:
        transport.close()


if __name__ == "__main__":
    # start scheduler with main as task
    asyncio.run(main())
