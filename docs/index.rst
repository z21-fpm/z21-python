.. Z21 Python documentation master file, created by
   sphinx-quickstart on Sat Mar  1 19:55:21 2025.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Z21 Python documentation
========================

Documentation about z21_python, which contains a Python library to
create programs using it.

Examples of application:

- simple monitoring from a computer application
- simple monitoring from an ESP32 device (e.g. a device with an
  integrated screen)

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered: 1
	      
   installation
   usage
   modules
