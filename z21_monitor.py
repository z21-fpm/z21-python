import sys
import socket
import argparse


# default address and port of the z21 central station
Z21_DEFAULT_ADDRESS = ("192.168.0.111", 21105)


class Z21(object):
    def __init__(self, address=Z21_DEFAULT_ADDRESS):
        # open udp (SOCK_DGRAM) socket to z21 central station (default: 192.268.0.111:21105)
        self.s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        # set address and port
        self.address = address

    def sendCommand(self, command):
        """
        Send specified command to socket
        """
        print("[sendCommand] command: {}".format(command.hex()))
        self.s.sendto(command, self.address)

    def slicePacket(self, data):
        """
        Slice packet into messages and handle every message
        """
        print("[slicePacket] data:      {}".format(data.hex()))
        while len(data) > 0:
            dataLen = int.from_bytes(data[0:2], byteorder="little")
            self.handleMessage(data[0:dataLen])
            data = data[dataLen:]

    def handleMessage(self, message):
        """
        Process a message
        """
        print("[handleMessage] message: {}".format(message.hex()))

        if message[2] == 0x84:
            # 2.18 LAN_SYSTEMSTATE_DATACHANGED
            print("[handleMessage] 2.18 LAN_SYSTEMSTATE_DATACHANGED")

            # MainCurrent
            main_current = int.from_bytes(message[4:6], byteorder="little")
            print("main_current: {} mA".format(main_current))

            # ProgCurrent
            prog_current = int.from_bytes(message[6:8], byteorder="little")

            # FilteredMainCurrent
            filtered_main_current = int.from_bytes(message[8:10], byteorder="little")

            # Temperature
            temperature = int.from_bytes(message[10:12], byteorder="little")
            print("temperature: {} C".format(temperature))

            # SupplyVoltage
            supply_voltage = int.from_bytes(message[12:14], byteorder="little")
            print("supply_voltage: {} mV".format(supply_voltage))

            # VCCVoltage
            vcc_voltage = int.from_bytes(message[14:16], byteorder="little")
            print("vcc_voltage: {} mV".format(vcc_voltage))

            # CentralState
            central_state = message[16]
            # print("central_state: {}".format(central_state.hex()))

        else:
            pass

    def lanGetSerialNumber(self):
        """
        Get serial number
        # 2.1 LAN_GET_SERIAL_NUMBER
        """
        print("[lanGetSerialNumber]")
        dataLen = int.to_bytes(0x0004, 2, byteorder="little")
        header = int.to_bytes(0x0010, 2, byteorder="little")
        command = dataLen + header

        self.sendCommand(command)

        # receive and process response
        incomingPacket, sender = self.s.recvfrom(1024)
        # print(incomingPacket)
        serial_number = int.from_bytes(incomingPacket[4:], byteorder="little")

        print("[lanGetSerialNumber] serial_number: {}".format(serial_number))
        return serial_number

    def lanSystemStateGetdata(self):
        """
        2.19 LAN_SYSTEMSTATE_GETDATA
        """
        print("[lanSystemStateGetdata]")
        dataLen = int.to_bytes(0x0004, 2, byteorder="little")
        header = int.to_bytes(0x0085, 2, byteorder="little")
        command = dataLen + header

        self.sendCommand(command)

        # receive and process response
        incomingPacket, sender = self.s.recvfrom(1024)
        self.slicePacket(incomingPacket)


def main(args):
    parser = argparse.ArgumentParser(description="z21 monitor")

    # parser.add_argument("first_parameter", type=int, help="First parameter")
    # parser.add_argument("second_parameter", type=int, help="Second parameter")

    parsed_args = parser.parse_args(args)
    # print(parsed_args)

    # main object
    z21 = Z21()

    serial_number = z21.lanGetSerialNumber()
    z21.lanSystemStateGetdata()


if __name__ == "__main__":
    main(sys.argv[1:])
