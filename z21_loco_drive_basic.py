import socket

# basic example to control a loco from z21 central station
# https://www.marcelvanoosterwijk.com/controlling-a-loc-using-the-roco-z21-and-python/

# z21 lan protocol can be downloaded from:
# https://www.z21.eu/en/downloads/manuals
# https://www.z21.eu/media/Kwc_Basic_DownloadTag_Component/root-en-main_47-1652-959-downloadTag-download/default/d559b9cf/1646977702/z21-lan-protokoll-en.pdf

# byteorder is positional to be compatible with micropython
# https://docs.micropython.org/en/latest/library/builtins.html#int.from_bytes
BYTEORDER = "little"

def lanXSetLocoFunction(address, function, switchMode):
    # 4.3 LAN_X_SET_LOCO_FUNCTION
    # function: decimal, 1 - 63 (should not be more than 28)
    # switchMode: 'on', 'off' or 'switch'
    dataLen = int.to_bytes(0x000A, 2, BYTEORDER)
    header = int.to_bytes(0x0040, 2, BYTEORDER)
    xHeader = int.to_bytes(0xE4, 1, BYTEORDER)
    db0 = int.to_bytes(0xF8, 1, BYTEORDER)
    db1 = int.to_bytes(0, 1, BYTEORDER)
    db2 = int.to_bytes(address, 1, BYTEORDER)
    # merge switchmode into function: overrule first 2 bits depending on switch mode:
    if switchMode == "off":  # bits 1 and 2: 00
        function = function & (~0b11000000)  # clear bits 1 and 2
    if switchMode == "on":  # bits 1 and 2: 01
        function = function & ~0b10000000  # clear bit 1
        function = function | 0b01000000  # set bit 2
    if switchMode == "switch":  # bits 1 and 2: 10
        function = function | 0b10000000  # set bit 1
        function = function & ~0b01000000  # clear bit 2
    db3 = int.to_bytes(function, 1, BYTEORDER)
    xor = 0x00E3 ^ 0x00F8 ^ 0 ^ address ^ function
    xorByte = int.to_bytes(xor, 1, BYTEORDER)
    data = xHeader + db0 + db1 + db2 + db3 + xorByte
    command = dataLen + header + data

    s.sendto(command, Z21)


def lanXSetLocoDrive(address, direction, res, speed):
    # 4.2 LAN_X_SET_LOCO_DRIVE
    # direction: 'forward' or 'backward'
    # res: 14, 28 or 128 steps (resolution)
    # speed: 0 - 13/27/127 (depending on res)
    dataLen = int.to_bytes(0x000A, 2, BYTEORDER)
    header = int.to_bytes(0x0040, 2, BYTEORDER)
    xHeader = int.to_bytes(0xE4, 1, BYTEORDER)
    if res == 14:
        binres = 16
    elif res == 28:
        binres = 18
    else:
        binres = 19
    db0 = int.to_bytes(binres, 1, BYTEORDER)
    db1 = int.to_bytes(0, 1, BYTEORDER)
    db2 = int.to_bytes(address, 1, BYTEORDER)
    # merge direction into speed: overrule first bit depending on direction:
    if direction == "forward":
        speed = speed | 0b10000000  # set bit 1
    else:
        speed = speed & ~0b10000000  # clear bit 1
    db3 = int.to_bytes(speed, 1, BYTEORDER)
    xor = 0x00E4 ^ binres ^ 0 ^ address ^ speed
    xorByte = int.to_bytes(xor, 1, BYTEORDER)
    data = xHeader + db0 + db1 + db2 + db3 + xorByte
    command = dataLen + header + data

    s.sendto(command, Z21)


# open udp (SOCK_DGRAM) socket to z21 central station (default: 192.268.0.111:21105)
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
Z21 = ("192.168.0.111", 21105)

# loco
loco_addresss = 5

# lights on
lanXSetLocoFunction(loco_addresss, 0, "on")

# forward with speed 50/128
lanXSetLocoDrive(loco_addresss, "forward", 128, 50)

# close socket
s.close()
