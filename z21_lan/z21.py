# header
LAN_GET_SERIAL_NUMBER = "1000"
LAN_GET_CODE = "1800"
LAN_GET_HWINFO = "1a00"
LAN_LOGOFF = "3000"

# X-BUS tunneling
LAN_X_TUNNELING = "4000"
LAN_X_GET_VERSION = "40002121"
LAN_X_GET_STATUS = "40002124"
LAN_X_SET_TRACK_POWER_OFF = "40002180"
LAN_X_SET_TRACK_POWER_ON = "40002181"
LAN_X_DCC_READ_REGISTER = "40002211"
LAN_X_CV_READ = "40002311"
LAN_X_DCC_WRITE_REGISTER = "40002312"
LAN_X_CV_WRITE = "40002412"
LAN_X_MM_WRITE_BYTE = "400024ff"
LAN_X_GET_TURNOUT_INFO = "400043"
LAN_X_TURNOUT_INFO = "400043"
LAN_X_GET_EXT_ACCESSORY_INFO = "400044"
LAN_X_EXT_ACCESSORY_INFO = "400044"
LAN_X_SET_TURNOUT = "400053"
LAN_X_SET_EXT_ACCESSORY = "400054"
LAN_X_BC_TRACK_POWER_OFF = "40006100"
LAN_X_BC_TRACK_POWER_ON = "40006101"
LAN_X_BC_PROGRAMMING_MODE = "40006102"
LAN_X_BC_TRACK_SHORT_CIRCUIT = "40006108"
LAN_X_CV_NACK_SC = "40006112"
LAN_X_CV_NACK = "40006113"
LAN_X_UNKNOWN_COMMAND = "40006182"
LAN_X_STATUS_CHANGED = "40006222"
LAN_X_GET_VERSION_RESPONSE = "40006321"
LAN_X_CV_RESULT = "40006414"
LAN_X_SET_STOP = "400080"
LAN_X_BC_STOPPED = "40008100"
LAN_X_SET_LOCO_E_STOP = "400092"
LAN_X_PURGE_LOCO = "4000e344"
LAN_X_GET_LOCO_INFO = "4000e3f0"
LAN_X_SET_LOCO_DRIVE = "4000e4"
LAN_X_SET_LOCO_FUNCTION = "4000e4f8"
LAN_X_SET_LOCO_FUNCTION_GROUP = "4000e4"
LAN_X_SET_LOCO_BINARY_STATE = "4000e45f"
LAN_X_CV_POM_WRITE_BYTE = "4000e630"
LAN_X_CV_POM_WRITE_BIT = "4000e630"
LAN_X_CV_POM_READ_BYTE = "4000e630"
LAN_X_CV_POM_ACCESSORY_WRITE_BYTE = "4000e631"
LAN_X_CV_POM_ACCESSORY_WRITE_BIT = "4000e631"
LAN_X_CV_POM_ACCESSORY_READ_BYTE = "4000e631"
LAN_X_LOCO_INFO = "4000ef"
LAN_X_GET_FIRMWARE_VERSION = "4000f10a"
LAN_X_GET_FIRMWARE_VERSION_RESPONSE = "4000f30a"

LAN_SET_BROADCASTFLAGS = "5000"
LAN_GET_BROADCASTFLAGS = "5100"
LAN_GET_BROADCASTFLAGS_RESPONSE = "5100"
LAN_GET_LOCOMODE = "6000"
LAN_GET_LOCOMODE_RESPONSE = "6000"
LAN_SET_LOCOMODE = "6100"
LAN_GET_TURNOUTMODE = "7000"
LAN_GET_TURNOUTMODE_RESPONSE = "7000"
LAN_SET_TURNOUTMODE = "7100"
LAN_RMBUS_DATACHANGED = "8000"
LAN_RMBUS_GETDATA = "8100"
LAN_RMBUS_PROGRAMMODULE = "8200"
LAN_SYSTEMSTATE_DATACHANGED = "8400"
LAN_SYSTEMSTATE_GETDATA = "8500"
LAN_RAILCOM_DATACHANGED = "8800"
LAN_RAILCOM_GETDATA = "8900"
LAN_LOCONET_Z21_RX = "a000"
LAN_LOCONET_Z21_TX = "a100"
LAN_LOCONET_FROM_LAN = "a200"
LAN_LOCONET_DISPATCH_ADDR = "a300"
LAN_LOCONET_DETECTOR = "a400"
LAN_CAN_DETECTOR = "c400"
LAN_CAN_DEVICE_GET_DESCRIPTION = "c800"
LAN_CAN_DEVICE_GET_DESCRIPTION_RESPONSE = "c800"
LAN_CAN_DEVICE_SET_DESCRIPTION = "c900"
LAN_CAN_BOOSTER_SYSTEMSTATE_CHGD = "ca00"
LAN_CAN_BOOSTER_SET_TRACKPOWER = "cb00"
LAN_FAST_CLOCK_DATA = "cd00"
LAN_FAST_CLOCK_SETTINGS_GET = "ce00"
LAN_BOOSTER_GET_DESCRIPTION_RESPONSE = "b800"
LAN_BOOSTER_SYSTEMSTATE_DATACHANGED = "ba00"
LAN_DECODER_GET_DESCRIPTION_RESPONSE = "d800"
LAN_DECODER_SYSTEMSTATE_DATACHANGED = "da00"
LAN_ZLINK_GET_HWINFO = "e80006"
LAN_FAST_CLOCK_CONTROL = "cc00"
LAN_FAST_CLOCK_SETTINGS_GET = "ce00"
LAN_FAST_CLOCK_SETTINGS_SET = "cf00"
LAN_BOOSTER_SET_POWER = "b200"
LAN_BOOSTER_GET_DESCRIPTION = "b800"
LAN_BOOSTER_SET_DESCRIPTION = "b900"
LAN_BOOSTER_SYSTEMSTATE_GETDATA = "bb00"
LAN_DECODER_GET_DESCRIPTION = "d800"
LAN_DECODER_SET_DESCRIPTION = "d900"
LAN_DECODER_SYSTEMSTATE_GETDATA = "db00"
LAN_ZLINK_GET_HWINFO = "e800"

# 2.16 LAN_SET_BROADCASTFLAGS
FLAG_DRIVING_SWITCHING = 0x00000001
FLAG_RBUS = 0x00000002
FLAG_RAILCOM = 0x00000004
FLAG_SYSTEMSTATUS = 0x00000100
FLAG_ALL_LOCOS = 0x00010000
FLAG_LOCONET = 0x01000000
FLAG_LOCONET_LOCO_SPECIFIC = 0x02000000
FLAG_LOCONET_SWITCH_SPECIFIC = 0x04000000
FLAG_LOCONET_OCCUPANCY = 0x08000000
FLAG_RAILCOM_ALL_LOCOS = 0x00040000
FLAG_CANBUS_OCCUPANCY = 0x00080000
FLAG_CANBUS_BOOSTER = 0x00020000
FLAG_FAST_CLOCK = 0x00000010

# byteorder is positional to be compatible with micropython
# https://docs.micropython.org/en/latest/library/builtins.html#int.from_bytes
BYTEORDER_LITTLE = "little"
BYTEORDER_BIG = "big"


def z21_encode(header_hex, **kwargs):
    """Encode to z21 data record.

    Data record has the structure:
    DataLen (2 bytes) + Header (2 bytes) + Data (n bytes)
    Ref: 1.2 Z21 Dataset
    """
    header_bytes = bytes.fromhex(header_hex)
    data_bytes = kwargs.get("data", b"")
    header_data_bytes = header_bytes + data_bytes
    data_len_bytes = int.to_bytes(len(header_data_bytes) + 2, 2, BYTEORDER_LITTLE)

    data_record = data_len_bytes + header_bytes + data_bytes
    print("[z21_encode] data_record: {}".format(data_record))
    return data_record


def z21_decode(data_record):
    """Decode from z21 data record.

    Ref: 1.2 Z21 Dataset
    """
    data_record_hex = data_record.hex()

    # 2-bytes (little-endian): DataLen
    # 2-bytes (little-endian): Header
    # n-bytes (DataLen-4): Data (last byte is XOR-byte)

    # dataLen
    data_len = data_record[0:2]
    data_len_hex = data_len.hex()

    # Header
    header = data_record[2:4]
    header_hex = header.hex()

    # Data
    data = data_record[4:]
    data_hex = data.hex()

    print(
        "[z21_decode] {} -> data_len: {}, header: {}, data: {}".format(
            data_record_hex, data_len_hex, header_hex, data_hex
        )
    )

    # LAN_GET_SERIAL_NUMBER 1000
    if header_hex == LAN_GET_SERIAL_NUMBER:
        serial_number = int.from_bytes(data, BYTEORDER_LITTLE)
        print("[z21_decode] LAN_GET_SERIAL_NUMBER: {}".format(serial_number))

    # LAN_GET_CODE 1800
    elif header_hex == LAN_GET_CODE:
        code_hex = int.to_bytes(data[-1], 1, BYTEORDER_BIG).hex()
        code_dict = {
            "00": "all features permitted",
            "01": "z21 start: driving and switching is blocked",
            "02": "z21 start: driving and switching is permitted",
        }
        print("[z21_decode] LAN_GET_CODE: {}".format(code_dict[code_hex]))

    # LAN_X_TUNNELING
    elif header_hex == LAN_X_TUNNELING:
        # Header + X-Header
        header_x_header = data_record[2:5]
        header_x_header_hex = header_x_header.hex()

        # Header + X-Header + DB0
        header_x_header_db0 = data_record[2:6]
        header_x_header_db0_hex = header_x_header_db0.hex()

        # X-Header
        x_header = data[0:1]
        x_header_hex = x_header.hex()

        # X-Header + DB0
        x_header_db0 = data[0:2]
        x_header_db0_hex = x_header_db0.hex()

        # DB0 + DB1 + DB2 + ...
        db = data[1:]
        db_hex = db.hex()
        print("[z21_decode] db_hex: {}".format(db_hex))

        # LAN_X_TURNOUT_INFO 400043
        if header_x_header_hex == LAN_X_TURNOUT_INFO:
            db0_db1 = data[1:3]
            db0_db1_int = int.from_bytes(db0_db1, BYTEORDER_BIG)
            db2 = data[3:4]
            db2_hex = db2.hex()
            print(
                "[z21_decode] LAN_X_TURNOUT_INFO FAdr: {}, DB2: {}".format(
                    db0_db1_int, db2_hex
                )
            )

        # LAN_X_BC_TRACK_POWER_OFF 40006100
        elif header_x_header_db0_hex == LAN_X_BC_TRACK_POWER_OFF:
            print("[z21_decode] LAN_X_BC_TRACK_POWER_OFF")

        # LAN_X_BC_TRACK_POWER_ON 40006101
        elif header_x_header_db0_hex == LAN_X_BC_TRACK_POWER_ON:
            print("[z21_decode] LAN_X_BC_TRACK_POWER_ON")

        # LAN_X_GET_VERSION_RESPONSE 40006321
        elif header_x_header_db0_hex == LAN_X_GET_VERSION_RESPONSE:
            x_bus_version_major = data_hex[4]
            x_bus_version_minor = data_hex[5]
            print(
                "[z21_decode] LAN_X_GET_VERSION XBUS_VER: V{}.{}".format(
                    x_bus_version_major, x_bus_version_minor
                )
            )
            command_station_id = int.to_bytes(data[-2], 1, BYTEORDER_BIG).hex()
            print(
                "[z21_decode] LAN_X_GET_VERSION CMDST_ID: {}".format(command_station_id)
            )

        # LAN_X_BC_STOPPED 40008100
        elif header_x_header_db0_hex == LAN_X_BC_STOPPED:
            print("[z21_decode] LAN_X_BC_STOPPED")

        # LAN_X_LOCO_INFO 4000ef
        elif header_x_header_hex == LAN_X_LOCO_INFO:
            print("[z21_decode] LAN_X_LOCO_INFO")

            # DB0,DB1: Adr_MSB, Adr_LSB
            db0_db1 = db[0:2]
            db0_db1_int = int.from_bytes(db0_db1, BYTEORDER_BIG)
            print("[z21_decode] LAN_X_LOCO_INFO Adr: {}".format(db0_db1_int))

            # DB2
            db2 = db[2]
            speedSteps = db2 & 0b00000111
            print("[z21_decode] LAN_X_LOCO_INFO speedSteps: {}".format(speedSteps))

            # DB3
            db3 = db[3]
            direction = (db3 & 0b10000000) >> 7
            # direction: 1=forward
            print("[z21_decode] LAN_X_LOCO_INFO direction: {}".format(direction))
            speed = db3 & 0b01111111
            print("[z21_decode] LAN_X_LOCO_INFO speed: {}".format(speed))

            db4 = db[4]
            db5 = db[5]
            db6 = db[6]
            db7 = db[7]
            db8 = db[8]

        # LAN_X_GET_FIRMWARE_VERSION_RESPONSE 4000f30a
        elif header_x_header_db0_hex == LAN_X_GET_FIRMWARE_VERSION_RESPONSE:
            db1_hex = int.to_bytes(data[-3], 1, BYTEORDER_BIG).hex()
            db2_hex = int.to_bytes(data[-2], 1, BYTEORDER_BIG).hex()
            print(
                "[z21_decode] LAN_X_GET_FIRMWARE_VERSION: {}.{}".format(
                    db1_hex, db2_hex
                )
            )

        else:
            print("[z21_decode] LAN_X_TUNNELING: unimplemented")

    # LAN_GET_HWINFO 1a00
    elif header_hex == LAN_GET_HWINFO:
        hw_type = data[0:4]
        hw_type_hex = hw_type.hex()
        hw_type_dict = {
            "00020000": "black Z21 (2012)",
            "01020000": "black Z21",
            "02020000": "SmartRail",
            "03020000": "white z21",
            "04020000": "z21 start",
            "05020000": "10806 Z21 Single Booster",
            "06020000": "10807 Z21 Dual Booster",
            "11020000": "10870 Z21 XL Series",
            "12020000": "10869 Z21 XL Booster",
            "01030000": "10836 Z21 SwitchDecoder",
            "01030000": "10836 Z21 SignalDecoder",
        }
        print(
            "[z21_decode] LAN_GET_HWINFO HWType: {}".format(hw_type_dict[hw_type_hex])
        )

        fw_version = data[4:8]
        db1_hex = int.to_bytes(fw_version[1], 1, BYTEORDER_BIG).hex()
        db2_hex = int.to_bytes(fw_version[0], 1, BYTEORDER_BIG).hex()
        print("[z21_decode] LAN_GET_HWINFO FW version: {}.{}".format(db1_hex, db2_hex))

    # LAN_GET_BROADCASTFLAGS 5100
    elif header_hex == LAN_GET_BROADCASTFLAGS:
        print("[z21_decode] LAN_GET_BROADCASTFLAGS")

    # LAN_SYSTEMSTATE_DATACHANGED 8400
    elif header_hex == LAN_SYSTEMSTATE_DATACHANGED:
        print("[z21_decode] LAN_SYSTEMSTATE_DATACHANGED")
        main_current = data[0:2]
        prog_current = data[2:4]
        filtered_main_current = data[4:6]
        temperature = data[6:8]
        supply_voltage = data[8:10]
        vcc_voltage = data[10:12]
        central_state = data[12:13]
        central_state_ex = data[13:14]
        reserved = data[14:15]
        capabilities = data[15:16]

        # main current
        main_current_int = int.from_bytes(main_current, BYTEORDER_LITTLE)
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: MainCurrent={} mA".format(
                main_current_int
            )
        )
        # prog current
        prog_current_int = int.from_bytes(prog_current, BYTEORDER_LITTLE)
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: ProgCurrent={} mA".format(
                prog_current_int
            )
        )
        # filtered main current
        filtered_main_current_int = int.from_bytes(
            filtered_main_current, BYTEORDER_LITTLE
        )
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: FilteredMainCurrent={} mA".format(
                filtered_main_current_int
            )
        )
        # temperature
        temperature_int = int.from_bytes(temperature, BYTEORDER_LITTLE)
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: Temperature={} °C".format(
                temperature_int
            )
        )
        # supply voltage
        supply_voltage_int = int.from_bytes(supply_voltage, BYTEORDER_LITTLE)
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: SupplyVoltage={} mV".format(
                supply_voltage_int
            )
        )
        # VCC voltage
        vcc_voltage_int = int.from_bytes(vcc_voltage, BYTEORDER_LITTLE)
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: VCCVoltage={} mV".format(
                vcc_voltage_int
            )
        )
        # central state
        central_state_hex = central_state.hex()
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: CentralState={}".format(
                central_state_hex
            )
        )
        # central state ex
        central_state_ex_hex = central_state_ex.hex()
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: CentralStateEx={}".format(
                central_state_ex_hex
            )
        )
        # capabilities
        capabilities_hex = central_state_ex.hex()
        print(
            "[z21_decode] LAN_SYSTEMSTATE_DATACHANGED: Capabilities={}".format(
                capabilities_hex
            )
        )

    else:
        print("[z21_decode] unimplemented")
