""" Basic example to subscribe to z21 central station and get messages during 60 seconds

z21 lan protocol can be downloaded from:
https://www.z21.eu/en/downloads/manuals
https://www.z21.eu/media/Kwc_Basic_DownloadTag_Component/root-en-main_47-1652-959-downloadTag-download/default/d559b9cf/1646977702/z21-lan-protokoll-en.pdf
"""

import sys
import socket


# byteorder is positional to be compatible with micropython
# https://docs.micropython.org/en/latest/library/builtins.html#int.from_bytes
BYTEORDER = "little"


def handleMessage(message):
    print("[handleMessage] message: {}".format(message.hex()))
    if message[2] != 0x40 or message[4] != 0xEF:
        return
    # exclude DataLen (2 bytes), Header (2 bytes), X-Header (1 byte): 5 first bytes to be excluded
    db = message[5:]

    address = int.from_bytes(db[0:2], byteorder="big")
    speedSteps = db[2] & 0b00000111
    direction = db[3] & 0b10000000
    speed = db[3] & 0b01111111
    doubletraction = db[4] & 0b01000000
    smartsearch = db[4] & 0b00100000
    f0 = db[4] & 0b00010000
    f1 = db[4] & 0b00000001
    f2 = db[4] & 0b00000010
    f3 = db[4] & 0b00000100
    f4 = db[4] & 0b00001000
    f5 = db[5] & 0b00000001
    f6 = db[5] & 0b00000010
    f7 = db[5] & 0b00000100
    f8 = db[5] & 0b00001000
    f9 = db[5] & 0b00010000
    f10 = db[5] & 0b00100000
    f11 = db[5] & 0b01000000
    f12 = db[5] & 0b10000000
    f13 = db[6] & 0b00000001
    f14 = db[6] & 0b00000010
    f15 = db[6] & 0b00000100
    f16 = db[6] & 0b00001000
    f17 = db[6] & 0b00010000
    f18 = db[6] & 0b00100000
    f19 = db[6] & 0b01000000
    f20 = db[6] & 0b10000000
    f21 = db[7] & 0b00000001
    f22 = db[7] & 0b00000010
    f23 = db[7] & 0b00000100
    f24 = db[7] & 0b00001000
    f25 = db[7] & 0b00010000
    f26 = db[7] & 0b00100000
    f27 = db[7] & 0b01000000
    f28 = db[7] & 0b10000000


def slicePacket(data):
    print("[slicePacket] data:      {}".format(data.hex()))
    while len(data) > 0:
        dataLen = int.from_bytes(data[0:2], BYTEORDER)
        handleMessage(data[0:dataLen])
        data = data[dataLen:]


def main(args):
    # open udp (SOCK_DGRAM) socket to z21 central station (default: 192.268.0.111:21105)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    Z21 = ("192.168.0.111", 21105)

    # 2.16 LAN_SET_BROADCASTFLAGS
    dataLen = int.to_bytes(0x0008, 2, BYTEORDER)
    header = int.to_bytes(0x0050, 2, BYTEORDER)
    data = int.to_bytes(0x00010001, 4, BYTEORDER)
    command = dataLen + header + data
    s.sendto(command, Z21)

    # infinite loop (but Z21 will stop sending after 60 seconds)
    while True:
        incomingPacket, sender = s.recvfrom(1024)
        if sender != Z21:
            continue  # ignore message
        slicePacket(incomingPacket)

    # close socket
    s.close()


if __name__ == "__main__":
    main(sys.argv[1:])
