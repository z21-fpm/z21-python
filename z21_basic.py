""" Basic example to communicate to z21 central station and retrieve its serial number

https://www.marcelvanoosterwijk.com/connecting-to-the-roco-z21/
"""

import sys
import socket


def main(args):
    # z21 lan protocol can be downloaded from:
    # https://www.z21.eu/en/downloads/manuals
    # https://www.z21.eu/media/Kwc_Basic_DownloadTag_Component/root-en-main_47-1652-959-downloadTag-download/default/d559b9cf/1646977702/z21-lan-protokoll-en.pdf

    # byteorder is positional to be compatible with micropython
    # https://docs.micropython.org/en/latest/library/builtins.html#int.from_bytes
    BYTEORDER = "little"

    # open udp (SOCK_DGRAM) socket to z21 central station (default: 192.268.0.111:21105)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    Z21 = ("192.168.0.111", 21105)

    # send packet 2.1 LAN_GET_SERIAL_NUMBER
    command = b"\x04" + b"\x00" + b"\x10" + b"\x00"
    s.sendto(command, Z21)

    # receive and process response
    incomingPacket, sender = s.recvfrom(1024)
    print(incomingPacket)
    serialNumber = int.from_bytes(incomingPacket[4:], BYTEORDER)
    print("z21 serial number: {}".format(serialNumber))

    # close socket
    s.close()


if __name__ == "__main__":
    main(sys.argv[1:])
